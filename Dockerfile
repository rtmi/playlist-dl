ARG ARCH=

FROM ${ARCH}golang:1.14-alpine3.11 AS build-env
WORKDIR /app
COPY . .
RUN go install ./...

FROM ${ARCH}alpine:3.11
COPY --from=build-env /go/bin/lambda /bin/p9lsvc
COPY --from=build-env /go/bin/p9l /bin/p9lcmd

CMD ["/bin/p9lcmd"]
