// playlist tool for the CLI
package main

import (
	//"bytes"
	"context"
	//"encoding/json"
	"flag"
	"fmt"
	//"io"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"time"

	pldl "github.com/patterns/playlist-dl/catalog"
	"github.com/pkg/browser"
	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"
)

// composite of CLI fields that's unecessary in catalog namespace
type cliconf struct {
	uri     string
	quality string

	pldl.Config
}

// the redirect URI must be exact match of the registered field in the Twitch API console
const REDIRECT_URI = "http://localhost:8080/vod/"

var (
	pattern = regexp.MustCompile(
		`https?://(?:(?:(?:www|go|m)\.)?(?P<id>twitch)\.tv/|player\.(?P<id>twitch)\.tv/\?.*?\bchannel=)(?P<id>[^/#?]+)`)
	PAT_API_GAME = regexp.MustCompile(`https://api\.(?P<id>twitch)\.tv/helix/games\?name=(?P<id>[^/#?]+)`)
)

func main() {
	ctx := context.Background()
	conf := readArgs()

	if unknownURL(conf.uri) {
		panic(fmt.Errorf("Requested video URL is not supported - %s", conf.uri))
	}

	cl := pldl.NewCataloger(ctob(conf))

	conf.Network = platform(conf.uri)
	err := cl.PreOIDC(ctx)
	if err != nil {
		panic(fmt.Errorf("Prepare of OIDC workflow (authorization URLs) failed: %v", err))
	}

	//TODO refactor
	//     - old authorize is sequential. Now with middleware, the authorize is layered.

	//authorize(conf, cl)

	uri := formatURI(conf)
	u, err := url.Parse(uri)
	if err != nil {
		panic(fmt.Errorf("Malformed video URL %v", err))
	}

	vids := cl.Collection(u)
	summary(conf, vids)
}

func formatURI(conf *cliconf) string {
	p := path(conf)

	switch conf.Network {
	case "twitch":
		return fmt.Sprintf("https://localhost/api/twitch%s", p)

	case "youtube":
		return fmt.Sprintf("https://localhost/api/youtube%s", p)

	default:
		return ""
	}
}

// run oidc flow to obtain the access token required in Helix API calls
func authorize(conf *cliconf, cl pldl.Catalog) {
	if notAPI(conf.uri) {
		// OIDC flow can be skipped
		return
	}
	// need to call vendor-specific (Twitch) API
	tw := cl.(*pldl.Twitch)

	g, gcx := errgroup.WithContext(context.Background())
	done := make(chan int)
	// spawn a :8080 ready for when redirect sends chromedp to final step
	srv := &http.Server{Addr: ":8080"}
	g.Go(func() error {
		// we pass the done channel which lets the handler signal completion
		http.HandleFunc("/vod/", makeHandler(tokenHandler, done, tw))

		if err := srv.ListenAndServe(); err != nil {
			return errors.Wrap(err, "Failed to set tcp listener")
		}
		return nil
	})
	// start a goroutine to watch for auth-done signal
	g.Go(func() error {
		defer close(done)

		<-done
		// teardown listeners
		tcx, _ := context.WithTimeout(gcx, 5*time.Second)
		srv.Shutdown(tcx)
		return nil
	})

	// TODO expose scope/claims to allow configuration
	// helix api call requires prompting the user to visit Twitch auth sign-in first
	fmt.Println("Please authorize Twitch Helix API call")
	api, _ := tw.HelixAuth(REDIRECT_URI, false)
	browser.OpenURL(api)

	// block until goroutines finish
	if err := g.Wait(); err != nil {
		fmt.Printf("Group wait done - %v \n", err)
	}

	// request access token
	acc, err := tw.HelixAccess()
	if err != nil {
		panic(fmt.Errorf("DEBUG ac token error - %v \n", err))
	}
	fmt.Printf("DEBUG acc token - %s \n", acc.AccessToken)
	////tw.HelixToken(acc.AccessToken)
}

// middleware to parameterize cataloger
func makeHandler(fn func(http.ResponseWriter, *http.Request, *pldl.Twitch), sig chan int, cl *pldl.Twitch) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// pass catalog to handler
		fn(w, r, cl)
		if sig != nil {
			// signal httpd to shutdown
			sig <- 1
		}
	}
}
func tokenHandler(w http.ResponseWriter, r *http.Request, cl *pldl.Twitch) {

	fmt.Printf("DEBUG authdone header - %v \n", r.Header)
	// At this point, Twitch authorize redirect sent user here.
	// There should be a code (30 chars) in the querystring.
	code := r.FormValue("code")
	if len(code) != 30 {
		fmt.Printf("DEBUG code empty logic branch (could be error potentially) \n")
		return
	}

	// TODO state variable may help us track auth flow
	// Use the code to obtain a user access token
	cl.HelixCode(code)
	// close browser
	fmt.Fprintf(w, `<html><head><script>window.close();</script></head><body>Success! Close browser.</body></html>`)
}

// print to console
func summary(conf *cliconf, vids []pldl.Media) {
	if notAPI(conf.uri) {
		uri := chooseQuality(vids, conf.quality)
		fmt.Println(uri)
	} else {
		for k, v := range vids {
			fmt.Printf("%d. %s, %s, %s \n", k, v.Title, v.Quality, v.URL)
		}
	}
}

// filter for desired quality
func chooseQuality(formats []pldl.Media, quality string) string {
	var av []string
	for _, v := range formats {
		if v.Quality == quality {
			return v.URL
		}
		av = append(av, v.Quality)
	}

	fmt.Printf("Desired resolution not found. Quality options are %s \n", strings.Join(av, ","))
	return ""
}

// format REST-like path
func path(conf *cliconf) string {
	// only two known resources for now

	if notAPI(conf.uri) {
		name := channel(conf.uri)
		return fmt.Sprintf("/channels/%s", name)
	}

	// API games call
	key := resourceKey(conf.uri)
	return fmt.Sprintf("/games/%s", key)
}

func resourceKey(uri string) string {
	// extract resource key
	if unknownURL(uri) {
		return ""
	}
	subs := PAT_API_GAME.FindStringSubmatch(uri)
	if len(subs) == 3 {
		return subs[2]
	}
	return ""
}

func channel(uri string) string {
	// extract channel name
	if unknownURL(uri) {
		return ""
	}
	subs := pattern.FindStringSubmatch(uri)
	if len(subs) == 4 {
		return subs[3]
	}
	return ""
}

func platform(uri string) string {
	// extract network name

	if unknownURL(uri) {
		return ""
	}

	if notAPI(uri) {
		subs := pattern.FindStringSubmatch(uri)

		if len(subs) > 2 {
			if subs[1] != "" {
				// network is the first capture (both patterns)
				return subs[1]
			}
			if len(subs) > 3 {
				// second capture of the general pattern
				return subs[2]
			}
		}
	}

	subs := PAT_API_GAME.FindStringSubmatch(uri)
	if len(subs) > 1 {
		return subs[1]
	}

	return ""
}

func unknownURL(uri string) bool {
	// check not supported URLs
	return (!pattern.MatchString(uri) && notAPI(uri))
}

func notAPI(uri string) bool {
	return !PAT_API_GAME.MatchString(uri)
}

func readArgs() *cliconf {
	var conf cliconf

	// base fields
	flag.StringVar(&conf.ClientID, pldl.CLI_CID, "", "Client ID of registered app")
	flag.DurationVar(&conf.Timeout, pldl.CLI_TMO, 120*time.Second, "URL open timeout")
	flag.StringVar(&conf.HelixID, pldl.CLI_HID, "", "Helix ID for new API")
	flag.StringVar(&conf.HelixSecret, pldl.CLI_SEC, "", "Helix secret of registered app")

	// CLI specific
	flag.StringVar(&conf.uri, "url", "https://m.twitch.tv/mst3k", "URL to access or API (e.g., https://api.twitch.tv/helix/games?id=493057)")
	flag.StringVar(&conf.quality, "quality", "720p", "Video quality choice")

	flag.Parse()

	// helix ID is the client ID in new Twitch API
	if conf.ClientID == "" {
		conf.ClientID = conf.HelixID
	}
	return &conf
}

// "cast" CLI to the base config
func ctob(c *cliconf) *pldl.Config {
	return &pldl.Config{
		ClientID:    c.ClientID,
		HelixID:     c.HelixID,
		HelixSecret: c.HelixSecret,
		Timeout:     c.Timeout,
		Network:     platform(c.uri),
	}
}
