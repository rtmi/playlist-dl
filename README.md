# playlist-dl
Yet Another Youtube-dl clone

 In part, for the learning Golang journey, but also to deal with
 several annoyances from sinkholing DNS in deny-by-default<sup>1</sup> mode.


## Quickstart




## Credits
youtube-dl 
  by [yt-dl.org](https://github.com/ytdl-org/youtube-dl) ([LICENSE](https://github.com/ytdl-org/youtube-dl/blob/master/LICENSE))

Streamlink 
  by [Streamlink Team](https://github.com/streamlink/streamlink) ([LICENSE](https://github.com/streamlink/streamlink/blob/master/LICENSE))

How to Find the Video URL 
 by [Johannes Bader](https://johannesbader.ch/blog/find-video-url-of-twitch-tv-live-streams-or-past-broadcasts/)

How to Use Netlify to Deploy 
 by [Carl Johnson](https://blog.carlmjohnson.net/post/2020/how-to-host-golang-on-netlify-for-free/)

