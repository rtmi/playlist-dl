package catalog

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"net/url"
	//"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/alexedwards/scs/v2"
	oidc "github.com/coreos/go-oidc"
	"github.com/grafov/m3u8"
	"github.com/pkg/errors"
	"golang.org/x/oauth2"
)

type Twitch struct {
	network     string
	clientID    string
	netClient   *http.Client
	helixID     string
	helixSecret string
	helixRedir  string
	helixCode   string
	oidcIssuer  string
	oidcState   string
	provider    *oidc.Provider
	Sessions    *scs.SessionManager
}

type AccessTokenResponse struct {
	Token            string    `json:"token"`
	Sig              string    `json:"sig"`
	MobileRestricted bool      `json:"mobile_restricted"`
	ExpiresAt        time.Time `json:"expires_at"`
}

type HelixTokenResponse struct {
	AccessToken  string `json:"access_token"`
	IDToken      string `json:"id_token"`
	RefreshToken string `json:"refresh_token"`
	TokenType    string `json:"token_type"`
}

type GamesResponse struct {
	Data []struct {
		ID        string `json:"id"`
		Name      string `json:"name"`
		BoxArtURL string `json:"box_art_url"`
	} `json:"data"`
}

type StreamsResponse struct {
	Data []struct {
		ID           string    `json:"id"`
		UserID       string    `json:"user_id"`
		UserName     string    `json:"user_name"`
		GameID       string    `json:"game_id"`
		Type         string    `json:"type"`
		Title        string    `json:"title"`
		ViewerCount  int       `json:"viewer_count"`
		StartedAt    time.Time `json:"started_at"`
		Language     string    `json:"language"`
		ThumbnailURL string    `json:"thumbnail_url"`
		TagIds       []string  `json:"tag_ids"`
	} `json:"data"`
	Pagination struct {
		Cursor string `json:"cursor"`
	} `json:"pagination"`
}

func NewTwitcher(conf *Config) Catalog {
	return &Twitch{
		network:     "twitch",
		clientID:    conf.clientID(),
		helixID:     conf.helixID(),
		helixSecret: conf.helixSecret(),
		helixRedir:  conf.helixRedir(),
		oidcIssuer:  "https://id.twitch.tv/oauth2",
		oidcState:   "foobar", // Don't do this in production.
		netClient:   &http.Client{Timeout: conf.httpTimeout()},
		Sessions:    conf.Sessions,
	}
}

// Network is the system type for the catalog interface
func (c *Twitch) Network() string {
	return c.network
}

// Playlist is the HTTP handler for Twitch API requests
func (c *Twitch) Playlist(w http.ResponseWriter, r *http.Request) {
	sesskey := r.URL.Query().Get("debug")
	scx, _ := c.Sessions.Load(r.Context(), sesskey)
	token := c.Sessions.GetString(scx, "Helix-Token")

	vids := c.assemble(path("twitch", r.URL.Path), token)
	buf := Marshal(vids)

	w.Header().Set("Content-Type", "application/json")
	w.Write(buf)
}

// Collection retrieves items according to the requested URL (not as handler which Playlist does)
func (c *Twitch) Collection(u *url.URL) []Media {
	fmt.Printf("DEBUG request url - %v \n", u)
	p := path("twitch", u.Path)
	fmt.Printf("DEBUG request path - %s \n", p)

	//TODO helix access token is used when the "games" API is requested
	vids := c.assemble(p, "")
	return vids
}

// TODO do we need "polymorph" Media type to handle different api results?
// assemble operates on path and returns the media list
func (c *Twitch) assemble(path string, token string) []Media {
	res, rt := c.resource(path)

	switch res {
	case "channels":
		return c.queue(rt)
	case "games":
		return c.mix(token, res, rt)
	default:
		// only support two resources for now
		var q []Media
		vid := Media{Title: "twitch placeholder", URL: "https://www.adafruit.com"}
		return append(q, vid)
	}
}

//TODO TODO TODO use regexp
func path(net, path string) string {
	i := calcPathIndex(net)
	return path[i:]
}

// calculate start index of resource path
func calcPathIndex(net string) int {
	return len("/api/" + net)
}

func (c *Twitch) queue(rt string) []Media {
	var q []Media

	name := c.resourceKey(rt)

	access, err := c.legacyToken(name)
	if err != nil {
		fmt.Printf("ERROR Downloading access token JSON - %v \n", err)
		return q
	}

	playlist, err := c.usher(access, name)
	if err != nil {
		fmt.Printf("ERROR Downloading playlist JSON - %v \n", err)
		return q
	}

	q = c.resolutions(playlist, name)
	return q
}

// mix pulls items related to the game category requested
func (c *Twitch) mix(token, res, rt string) []Media {
	var q []Media

	key := c.resourceKey(rt)

	gid, err := c.gameID(key, token)
	if err != nil {
		fmt.Printf("ERROR Downloading games JSON - %v \n", err)
		return q
	}
	ls, err := c.streams(gid, token)
	if err != nil {
		fmt.Printf("ERROR Downloading streams JSON - %v \n", err)
		return q
	}

	return c.stom(ls)
}

func (c *Twitch) PreOIDC(ctx context.Context) error {
	var err error
	c.provider, err = oidc.NewProvider(ctx, c.oidcIssuer)
	if err != nil {
		return err
	}
	return nil
}

func (c *Twitch) AuthCodeURL() string {
	config := oauth2.Config{
		ClientID:     c.helixID,
		ClientSecret: c.helixSecret,
		Endpoint:     c.provider.Endpoint(),
		RedirectURL:  c.helixRedir,
		Scopes:       []string{oidc.ScopeOpenID, "viewing_activity_read", "user:read:broadcast", "channel_feed_read"},
	}
	return config.AuthCodeURL(c.oidcState)
}

func (c *Twitch) HandleOAuth2Callback(w http.ResponseWriter, r *http.Request) {
	if r.URL.Query().Get("state") != c.oidcState {
		http.Error(w, "state did not match", http.StatusBadRequest)
		return
	}

	config := oauth2.Config{
		ClientID:     c.helixID,
		ClientSecret: c.helixSecret,
		Endpoint:     c.provider.Endpoint(),
		RedirectURL:  c.helixRedir,
		Scopes:       []string{oidc.ScopeOpenID, "viewing_activity_read", "user:read:broadcast", "channel_feed_read"},
	}

	oauth2Token, err := config.Exchange(r.Context(), r.URL.Query().Get("code"))
	if err != nil {
		http.Error(w, "Failed to exchange token: "+err.Error(), http.StatusInternalServerError)
		return
	}
	rawIDToken, ok := oauth2Token.Extra("id_token").(string)
	if !ok {
		http.Error(w, "No id_token field in oauth2 token.", http.StatusInternalServerError)
		return
	}

	oidcConfig := &oidc.Config{
		ClientID: config.ClientID,
	}
	verifier := c.provider.Verifier(oidcConfig)

	//idToken, err := verifier.Verify(ctx, rawIDToken)
	_, err = verifier.Verify(r.Context(), rawIDToken)
	if err != nil {
		http.Error(w, "Failed to verify ID Token: "+err.Error(), http.StatusInternalServerError)
		return
	}

	c.Sessions.Put(r.Context(), "Helix-Token", oauth2Token.AccessToken)
	sid, _, err := c.Sessions.Commit(r.Context())
	if err != nil {
		http.Error(w, "Failed to save session: "+err.Error(), http.StatusInternalServerError)
		return
	}

	// TODO send the client back to the route prior to the auth flow
	// don't use http.Redirect rather provide the client a navigation to trigger
	// OR dynamic generate/fill-template HTML
	spauri := fmt.Sprintf(`<a href="/twitch.html?debug=%s">Resume Search</a>`, sid)
	w.Write([]byte(spauri))
}

// oidc authorization code flow
func (c *Twitch) HelixAuth(redirect string, v5 bool) (api, anchor string) {
	// the user needs to allow app to act on their behalf by logging in on Twitch's sign-in
	// scope needs to indicate the things that will be accessed (viewing_activity)

	var cid = url.QueryEscape(c.helixID)
	if v5 {
		cid = url.QueryEscape(c.clientID)
	}
	api = fmt.Sprintf(
		"https://id.twitch.tv/oauth2/authorize?response_type=code&redirect_uri=%s&scope=viewing_activity_read+openid+user:read:broadcast+channel_feed_read&client_id=%s",
		redirect,
		cid)

	// construct HTML button that takes user to Twitch for authorization sign-in
	anchor = fmt.Sprintf(`<div><a id="startoidc" href="%s">Sign-in to Twitch and grant authorization for the app</a></div>`, api)

	return
}

// helix API token request
func (c *Twitch) HelixAccess() (HelixTokenResponse, error) {
	var acc HelixTokenResponse

	// use code to obtain user access token
	api := fmt.Sprintf(
		"https://id.twitch.tv/oauth2/token?redirect_uri=%s&client_id=%s&client_secret=%s&code=%s&grant_type=authorization_code",
		url.QueryEscape(c.helixRedir),
		url.QueryEscape(c.helixID),
		url.QueryEscape(c.helixSecret),
		url.QueryEscape(c.helixCode),
	)

	req, err := c.newRequest("POST", c.helixID, api, "", false, "")
	if err != nil {
		return acc, errors.Wrap(err, "Error creating Token HTTP request")
	}

	rsp, err := c.netClient.Do(req)
	if err != nil {
		return acc, errors.Wrap(err, "Error sending Token request")
	}
	defer rsp.Body.Close()

	d, err := json.NewDecoder(rsp.Body), nil
	if err != nil {
		return acc, errors.Wrap(err, "Error at JSON decoder")
	}

	err = d.Decode(&acc)
	if err != nil {
		return acc, errors.Wrap(err, "Error at Token unmarshal")
	}

	return acc, nil
}

// assign code
func (c *Twitch) HelixCode(code string) {
	c.helixCode = code
}

func (c *Twitch) streams(id string, token string) (StreamsResponse, error) {
	var str StreamsResponse
	// use id to obtain streams
	api := fmt.Sprintf("https://api.twitch.tv/helix/streams?game_id=%s", url.QueryEscape(id))

	req, err := c.newRequest("GET", c.helixID, api, "", false, token)
	if err != nil {
		return str, errors.Wrap(err, "Error creating Streams request")
	}

	rsp, err := c.netClient.Do(req)
	if err != nil {
		return str, errors.Wrap(err, "Error sending Streams request")
	}
	defer rsp.Body.Close()

	d, err := json.NewDecoder(rsp.Body), nil
	if err != nil {
		return str, errors.Wrap(err, "Error streams decoder")
	}

	err = d.Decode(&str)
	if err != nil {
		return str, errors.Wrap(err, "Error at streams unmarshal")
	}
	return str, nil
}

func (c *Twitch) gameID(name string, token string) (string, error) {
	// use key to obtain game data
	api := fmt.Sprintf("https://api.twitch.tv/helix/games?name=%s", url.QueryEscape(name))

	req, err := c.newRequest("GET", c.helixID, api, "", false, token)
	if err != nil {
		return "", errors.Wrap(err, "Error creating Game request")
	}

	rsp, err := c.netClient.Do(req)
	if err != nil {
		return "", errors.Wrap(err, "Error sending Game request")
	}
	defer rsp.Body.Close()

	d, err := json.NewDecoder(rsp.Body), nil
	if err != nil {
		return "", errors.Wrap(err, "Error games decoder")
	}
	var gam GamesResponse
	err = d.Decode(&gam)
	if err != nil {
		return "", errors.Wrap(err, "Error at games unmarshal")
	}
	if len(gam.Data) == 0 {
		return "", errors.Wrap(err, "Zero results match")
	}
	return gam.Data[0].ID, nil
}

func (c *Twitch) stom(streams StreamsResponse) []Media {
	// shape streams into a media list
	var q []Media

	for _, v := range streams.Data {
		label := fmt.Sprintf("%s - %s", v.UserName, v.Title)
		count := fmt.Sprintf("%d", v.ViewerCount)
		smed := Media{Title: label, URL: v.ThumbnailURL, Quality: count}
		q = append(q, smed)
	}

	return q
}

func (c *Twitch) usher(access AccessTokenResponse, name string) (*m3u8.MasterPlaylist, error) {
	rand.Seed(time.Now().UnixNano())

	// make query key/value pairs
	var q = url.Values{}
	q.Set("allow_source", "true")
	q.Set("allow_audio_only", "true")
	q.Set("allow_spectre", "false")
	q.Set("player", "twitchweb")
	////q.Set("playlist_include_framerate", "true")
	////q.Set("segment_preference", "4")
	q.Set("type", "any")
	q.Set("p", strconv.Itoa(rand.Intn(10000000-1000000)+1000000))
	q.Set("sig", access.Sig)
	q.Set("token", access.Token)

	uri := fmt.Sprintf(
		"https://usher.ttvnw.net/api/channel/hls/%s.m3u8?%s",
		url.QueryEscape(name),
		q.Encode(),
	)

	var mp *m3u8.MasterPlaylist

	// make net request
	rdr, err := c.Get(uri, true)
	if err != nil {
		return mp, err
	}
	defer rdr.Close()

	// unmarshal the playlist
	pl, ltype, err := m3u8.DecodeFrom(rdr, false)
	if err != nil {
		return mp, err
	}

	//todo do we always require type master
	switch ltype {
	//case m3u8.MEDIA:
	//	mediapl := pl.(*m3u8.MediaPlaylist)

	case m3u8.MASTER:
		mp = pl.(*m3u8.MasterPlaylist)
	}

	return mp, nil
}

func (c *Twitch) legacyToken(name string) (AccessTokenResponse, error) {

	var access AccessTokenResponse

	uri := fmt.Sprintf(
		"https://api.twitch.tv/api/channels/%s/access_token",
		url.QueryEscape(name),
	)

	d, err := c.getJSON(uri)
	if err != nil {
		return access, err
	}

	err = d.Decode(&access)
	if err != nil {
		return access, err
	}

	return access, nil
}

// transform formats to media list
func (c *Twitch) resolutions(playlist *m3u8.MasterPlaylist, common string) []Media {
	var q []Media

	for _, v := range playlist.Variants {
		if len(v.Alternatives) != 0 {
			alt := v.Alternatives[0]
			label := fmt.Sprintf("%s: %s", common, v.Video)

			smed := Media{Title: label, URL: v.URI, Quality: alt.Name}
			q = append(q, smed)
		}
	}

	return q
}

func (c *Twitch) getJSON(uri string) (*json.Decoder, error) {
	var err error

	req, err := c.newRequest("GET", c.clientID, uri, "", true, "")
	if err != nil {
		return &json.Decoder{}, err
	}

	rsp, err := c.netClient.Do(req)
	if err != nil {
		return &json.Decoder{}, err
	}

	return json.NewDecoder(rsp.Body), nil
}

func (c *Twitch) Get(uri string, v5 bool) (io.ReadCloser, error) {
	var (
		err error
		cid string
	)

	if v5 {
		cid = c.clientID
	} else {
		cid = c.helixID
	}
	req, err := c.newRequest("GET", cid, uri, "", v5, "")
	if err != nil {
		return nil, err
	}

	rsp, err := c.netClient.Do(req)
	if err != nil {
		return nil, err
	}

	if rsp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("HTTP client rcvd response (%s) which is not handled. \n", rsp.Status)
	}

	return rsp.Body, nil
}

// include header fields
func (c *Twitch) newRequest(method, cid, uri, keyword string, v5 bool, token string) (*http.Request, error) {
	// add header fields
	// in order to track form fields
	// prior to auth flow and redirect back
	// and reconstitute form state
	// todo next refactor header into (map) param

	req, err := http.NewRequest(method, uri, nil)
	if err != nil {
		return req, errors.Wrap(err, "Error with new HTTP request")
	}

	req.Header.Add("Client-ID", cid)
	req.Header.Add("Search-Keyword", keyword)
	if v5 {
		req.Header.Add("Accept", "application/vnd.twitchtv.v5+json; charset=UTF-8")
	} else {
		if token != "" {
			req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))
		}
	}

	return req, nil
}

// extract key from path
func (c *Twitch) resourceKey(right string) string {
	name := strings.Trim(right, "/ ")
	return name
}

// extract resource from path
func (c *Twitch) resource(path string) (res, rt string) {
	left := path
	if strings.HasPrefix(path, "/") {
		left = path[1:]
	}

	i := strings.IndexRune(left, '/')
	if i == -1 {
		res = ""
		rt = path
		return
	}

	res = left[:i]
	rt = left[i:]
	return
}
