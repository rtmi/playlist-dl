package catalog

import (
	"flag"
	"os"
	"time"

	"github.com/alexedwards/scs/v2"
)

const (
	ENV_TMO = "HTTP_TIMEOUT"
	ENV_CID = "TWITCHCLIENT"
	ENV_HID = "TWITCH_HELIX"
	ENV_SEC = "TWITCH_SECRET"
	ENV_RED = "TWITCH_REDIRECT"

	CLI_TMO = "timeout"
	CLI_CID = "clientid"
	CLI_HID = "helixid"
	CLI_SEC = "helixsecret"
	CLI_RED = "helixredir"
)

// configuration fields
type Config struct {
	Network     string
	ClientID    string
	HelixID     string
	HelixSecret string
	HelixRedir  string
	Sessions    *scs.SessionManager

	Timeout time.Duration
}

// timeout of http client
func (c Config) httpTimeout() time.Duration {
	if c.hasArg(CLI_TMO) {
		// CLI arg exists
		return c.Timeout
	}

	setting, ok := os.LookupEnv(ENV_TMO)
	if !ok {
		// env var is undefined, fallback to default flag
		return c.Timeout
	}

	seconds, err := time.ParseDuration(setting)
	if err != nil {
		panic("Error in timeout env var " + err.Error())
	}
	return seconds
}

// client ID of API
func (c Config) clientID() string {
	if c.hasArg(CLI_CID) {
		return c.ClientID
	}

	setting, ok := os.LookupEnv(ENV_CID)
	if !ok {
		// env var is undefined, fallback to default flag
		return c.ClientID
	}
	return setting
}

// helix ID of API
func (c Config) helixID() string {
	if c.hasArg(CLI_HID) {
		return c.HelixID
	}

	setting, ok := os.LookupEnv(ENV_HID)
	if !ok {
		// env var is undefined, fallback to default flag
		return c.HelixID
	}
	return setting
}

// helix secret of registered app
func (c Config) helixSecret() string {
	if c.hasArg(CLI_SEC) {
		return c.HelixSecret
	}

	setting, ok := os.LookupEnv(ENV_SEC)
	if !ok {
		// env var is undefined, fallback to default flag
		return c.HelixSecret
	}
	return setting
}

// helix redirect of registered app
func (c Config) helixRedir() string {
	if c.hasArg(CLI_RED) {
		return c.HelixRedir
	}

	setting, ok := os.LookupEnv(ENV_RED)
	if !ok {
		// env var is undefined, fallback to default flag
		return c.HelixRedir
	}
	return setting
}

func (c Config) hasArg(name string) bool {
	// Check whether CLI arg was actually specified, not just default
	// important because CLI arg takes precedence
	var found = false
	flag.Visit(func(f *flag.Flag) {
		if f.Name == name {
			found = true
		}
	})
	return found
}
